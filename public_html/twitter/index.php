<?php
// error_reporting(E_ALL);
// ini_set('display_errors','On');

/**
 * Demonstration of the various OAuth flows. You would typically do this
 * when an unknown user is first using your application. Instead of storing
 * the token and secret in the session you would probably store them in a
 * secure database with their logon details for your website.
 *
 * When the user next visits the site, or you wish to act on their behalf,
 * you would use those tokens and skip this entire process.
 *
 * The Sign in with Twitter flow directs users to the oauth/authenticate
 * endpoint which does not support the direct message permission. To obtain
 * direct message permissions you must use the "Authorize Application" flows.
 *
 * Instructions:
 * 1) If you don't have one already, create a Twitter application on
 *      http://dev.twitter.com/apps
 * 2) From the application details page copy the consumer key and consumer
 *      secret into the place in this code marked with (YOUR_CONSUMER_KEY
 *      and YOUR_CONSUMER_SECRET)
 * 3) Visit this page using your web browser.
 *
 * @author themattharris
 */

require './tmhOAuth.php';
require './tmhUtilities.php';
$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => 'CnVsc6K8YgNTVOMVVpdIKg',
  'consumer_secret' => 'JC9UjLyiwyP3ku1uY8VosFDspkfIYQmoEyn4tIkW4',
));

$here = tmhUtilities::php_self();
session_start();

function outputError($tmhOAuth) {
  echo 'Error: ' . $tmhOAuth->response['response'] . PHP_EOL;
  tmhUtilities::pr($tmhOAuth);
}

// reset request?
if ( isset($_REQUEST['wipe'])) {
  session_destroy();
  header("Location: {$here}");

// already got some credentials stored?
} elseif ( isset($_SESSION['access_token']) ) {
  $tmhOAuth->config['user_token']  = $_SESSION['access_token']['oauth_token'];
  $tmhOAuth->config['user_secret'] = $_SESSION['access_token']['oauth_token_secret'];

  $code = $tmhOAuth->request('GET', $tmhOAuth->url('1/account/verify_credentials'));
  if ($code == 200) {
    $resp = json_decode($tmhOAuth->response['response']);
    
    //echo $resp->screen_name;
  	//echo "in";
  	header('LOCATION:http://melonheadapp.com/twitter/pic.php');
  	
  } else {
    outputError($tmhOAuth);
  }
// we're being called back by Twitter
} elseif (isset($_REQUEST['oauth_verifier'])) {
  $tmhOAuth->config['user_token']  = $_SESSION['oauth']['oauth_token'];
  $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

  $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
    'oauth_verifier' => $_REQUEST['oauth_verifier']
  ));

  if ($code == 200) {
    $_SESSION['access_token'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
    unset($_SESSION['oauth']);
    header("Location: {$here}");
  } else {
    outputError($tmhOAuth);
  }
// start the OAuth dance
} elseif ( isset($_REQUEST['authenticate']) || isset($_REQUEST['authorize']) ) {
  $callback = isset($_REQUEST['oob']) ? 'oob' : $here;

  $params = array(
    'oauth_callback'     => $callback
  );

  if (isset($_REQUEST['force_write'])) :
    $params['x_auth_access_type'] = 'write';
  elseif (isset($_REQUEST['force_read'])) :
    $params['x_auth_access_type'] = 'read';
  endif;

  $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);

  if ($code == 200) {
    $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
    $method = isset($_REQUEST['authenticate']) ? 'authenticate' : 'authorize';
    $force  = isset($_REQUEST['force']) ? '&force_login=1' : '';
    $authurl = $tmhOAuth->url("oauth/{$method}", '') .  "?oauth_token={$_SESSION['oauth']['oauth_token']}{$force}";
    
    //echo '<p>To complete the OAuth flow follow this URL: <a href="'. $authurl . '">' . $authurl . '</a></p>';
  	header('LOCATION:'.$authurl);
  } else {
    outputError($tmhOAuth);
  }
}


// echo $tmhOAuth->config['user_token'];
// echo  "<br /><Br />";
// echo $tmhOAuth->config['user_secret'];

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
  <head>
    <title>melon head app</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    
    <link rel="shortcut icon" type="image/x-icon" href="http://melonheadapp.com/assets/gfx/favicon.ico" />
    
    <link rel="stylesheet" type="text/css" href="http://melonheadapp.com/assets/css/melons.css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Bevan&v2" rel="stylesheet" type="text/css">
    
    
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    
    <style type="text/css">
      #t{
        
        font-size:18px;
        line-height: 24px;
        letter-spacing: 1.5px;
        font-weight:bold;
        padding-bottom:15px;
      }

      a{
        
        font-weight:bold;
      }

      #t-msg{
        width:450px;
        margin:0 auto;
        text-align:center;
        margin-top:45px;  
      }
    </style>
    
  
  </head>
  <body>

   <div id="t-msg">
    <p id="t">Oops. There was a problem setting your photo.</p>
    <p>If you'd like, please try again.</p>

    <p><br><br><a href="#" onclick="self.close();return(false);">Close Window</a></p>
    </div>  
    
  
  </body>
</html>
 