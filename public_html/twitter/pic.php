<?php session_start(); ?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
  <head>
    <title>melon head app</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    
    <link rel="shortcut icon" type="image/x-icon" href="http://melonheadapp.com/assets/gfx/favicon.ico" />
    
    <link rel="stylesheet" type="text/css" href="http://melonheadapp.com/assets/css/melons.css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Bevan&v2" rel="stylesheet" type="text/css">
    
    
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    
    <style type="text/css">
      #t{
        
        font-size:18px;
        line-height: 24px;
        letter-spacing: 1.5px;
        font-weight:bold;
        padding-bottom:15px;
      }

      a{
        
        font-weight:bold;
      }

      #t-msg{
        width:450px;
        margin:0 auto;
        text-align:center;
        margin-top:45px;  
      }
    </style>
    
  
  </head>
  <body>





<?php


	function imagecreatefromfile($imagepath=false) {
    	
    	if(!$imagepath || !is_readable($imagepath)){
    		return false;
    	}
    	else{
    		return base64_encode(file_get_contents($imagepath));
		}
		
	} 

/**
 * Update the users profile image, or profile background image using OAuth.
 *
 * Although this example uses your user token/secret, you can use
 * the user token/secret of any user who has authorised your application.
 *
 * Instructions:
 * 1) If you don't have one already, create a Twitter application on
 *      http://dev.twitter.com/apps
 * 2) From the application details page copy the consumer key and consumer
 *      secret into the place in this code marked with (YOUR_CONSUMER_KEY
 *      and YOUR_CONSUMER_SECRET)
 * 4) Visit the 'My Access Token' screen linked to from your application
 *      details page
 * 5) Copy the user token and user secret into the place in this code marked
 *      with (A_USER_TOKEN and A_USER_SECRET)
 * 6) Visit this page using your web browser.
 *
 * @author themattharris
 */



  require './tmhOAuth.php';
  require './tmhUtilities.php';
  $tmhOAuth = new tmhOAuth(array(
    'consumer_key'    => 'CnVsc6K8YgNTVOMVVpdIKg',
    'consumer_secret' => 'JC9UjLyiwyP3ku1uY8VosFDspkfIYQmoEyn4tIkW4',
    'user_token'      => $_SESSION['access_token']['oauth_token'],
    'user_secret'     => $_SESSION['access_token']['oauth_token_secret'],
  ));

 
  
  $tumbnailExtention = preg_replace('/^.*\.([^.]+)$/D', '$1', $_SESSION['twitpic']['path']);
	$params = array('image' => '@' . $_SESSION['twitpic']['path'] . ';type=image/'.$tumbnailExtention.';filename='.$_SESSION['twitpic']['name']);

  
  $_POST['method'] = 'update_profile_image';

  // if we are setting the background we want it to be displayed
  if ($_POST['method'] == 'update_profile_background_image')
    $params['use'] = 'true';


  $code = $tmhOAuth->request('POST', $tmhOAuth->url("1/account/{$_POST['method']}"),
    $params,
    true, // use auth
    true  // multipart
  );

  //echo '<br>Code: '.$code.'<br>';

  if ($code == 200) {
   
    
    // $codea = $tmhOAuth->request('GET', $tmhOAuth->url('1/account/verify_credentials'));

    // if ($codea == 200) {
    
    //   $r = json_decode($tmhOAuth->response['response']);
     
    // } else {
      
    // }

    ?>
    <!-- SUCCESS -->
    <div id="t-msg">
    <p id="t">We've set your new profile<br>picture on Twitter.</p>
    <p>It may take a few minutes to show up at twitter.com</p>

    <p><br><br><a href="#" onclick="self.close();return(false);">Close Window</a></p>
    </div>  
    <?
  }
  else{
    ?>
    <div id="t-msg">
    <p id="t">Oops. There was a problem setting your photo.</p>
    <p>If you'd like, please try again.</p>

    <p><br><br><a href="#" onclick="self.close();return(false);">Close Window</a></p>
    </div>  
    <?
  }
  

  


?>



</body>
</html>
