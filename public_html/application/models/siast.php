<?php
class Siast extends CI_Model {

	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        
    }

	
	
	function createUser($fb_id,$name,$email,$location)
	{
		
		
				
		$data = array(
		   'fb_id' => $fb_id ,
		   'user_name' => $name ,
		   'user_email' => $email,
		   'user_location' => $location
		);
		
		
		$query = $this->db->insert('melonheads', $data); 
		
	}
	
	
	function checkUser($fb_id)
	{
		
		$query = $this->db->get_where('melonheads', array('fb_id' => $fb_id));
		$count = $query->num_rows();
		
		//echo "-->" . $count;
		
		if($count >= 1){
			return (TRUE);
		}else{
			return (FALSE);
		}	
	}
	
	
	function getBadges(){
		
		$this->db->order_by("id", "asc");
		$query = $this->db->get_where('siast_badges', array('active' => 'Y'), 5);
		 
		
		return $query;
	}
	
	
	function getBadgePng($id){
		//echo "-->" . $id;
		$query = $this->db->get_where('siast_badges', array('id' => $id));
		$row = $query->row(); 
		return $row->badge_png;
	}
	
	
	
	function saveTime($fb_id, $badge_id){
		
		try{
			$query = $this->db->query('UPDATE melonheads SET badges_made = (badges_made + 1), last_update = NOW() WHERE fb_id = '.$this->db->escape($fb_id).'');
		}
		catch(Exception $e){
			error_log('error save time - 1');
		}
		
		$data = array('user_id' => $fb_id,
					'badge_id' => $badge_id 
		);
		
		
		try{
			$this->db->insert('badge_usage', $data); 
		}
		catch(Exception $e){
			error_log('error save time - 2');
		}
		//echo "save " . 'UPDATE melonheads SET badges_made = (badges_made + 1), last_update = NOW() WHERE fb_id = "'.$this->db->escape($fb_id).'"';
	}


	function saveTimeBody($fb_id, $badge_id){
		
		try{
			$query = $this->db->query('UPDATE melonheads SET badges_made = (badges_made + 1), last_update = NOW() WHERE fb_id = '.$this->db->escape($fb_id).'');
		}
		catch(Exception $e){
			error_log('error save time - 1');
		}
		

		switch ($badge_id) {
			case '0':
				$badge_id = 5;
				break;
			case '1':
				$badge_id = 6;
				break;
			case '2':
				$badge_id = 7;
				break;
			case '3':
				$badge_id = 8;
				break;		
			default:
				$badge_id = $badge_id;
				break;
		}

		$data = array('user_id' => $fb_id,
					'badge_id' => $badge_id 
		);
		
		
		try{
			$this->db->insert('badge_usage', $data); 
		}
		catch(Exception $e){
			error_log('error save time - 2');
		}
		//echo "save " . 'UPDATE melonheads SET badges_made = (badges_made + 1), last_update = NOW() WHERE fb_id = "'.$this->db->escape($fb_id).'"';
	}
	
	
	/*
	function saveMemory($user_id,$friend_id,$memory_name,$memory_body,$memory_game){
		
		// check how many they have submmitted
		$check = $this->db->query("SELECT id FROM memories WHERE user_id = '".$user_id."' AND created LIKE '".date('Y-m-d')."%'");
		$count = $check->num_rows();
		if($count == 5){
			return("block");
		}	
		
		$memory_name = (trim(htmlspecialchars($memory_name, ENT_QUOTES)));
		$memory_body = (trim(htmlspecialchars($memory_body, ENT_QUOTES)));
		$memory_game = (trim(htmlspecialchars($memory_game, ENT_QUOTES)));
		
		$query = $this->db->query("INSERT INTO memories (user_id,friend_id,memory_name,memory_body,memory_game) VALUES ('".$user_id."',".$this->db->escape($friend_id).",".$this->db->escape($memory_name).",".$this->db->escape($memory_body).",".$this->db->escape($memory_game).")");
		
		return( $this->db->insert_id() );
		
	}
	
	
	function grabMemories($amount){
		
		return $this->db->query("SELECT * FROM memories ORDER BY RAND() LIMIT ".$this->db->escape($amount)."");		
	
	}
	
	function grabMemory($id){
		
		return $this->db->query("SELECT * FROM memories WHERE id = ".$this->db->escape($id)."");		
	
	}
	
	
	function userExists($fbid){
		
		$query = $this->db->query("SELECT * FROM users WHERE fb_id = '".$fbid."'");	
		$count = $query->num_rows();	
		
		return( $count );
	}
	
	
	function userCredits($fbid){
	
		$query = $this->db->query("SELECT * FROM memories WHERE user_id = '".$fbid."'");	
		$count = $query->num_rows();	
		
		return( $count );
	
	}
	
	function saveUser($user_id,$user_name,$user_location,$user_email){
		
		$query = $this->db->query("INSERT INTO users (fb_id,user_name,user_location,user_email) VALUES ('".$user_id."',".$this->db->escape($user_name).",".$this->db->escape($user_location).",".$this->db->escape($user_email).") ");
	
	}
	*/
	
	
	
	
	
	
	/* !ADMIN */
	
	function getUserStats(){
		
		$query = $this->db->query('SELECT count(id) as total_users, SUM(badges_made) as total_badges FROM melonheads');
		
		return( $query->row() );
		
	
	}
	
	
	function getUserList(){
		return $this->db->query('SELECT fb_id,user_name,user_email,user_location,badges_made,created as last_update FROM melonheads');
	}
	
	
	function getAllBadges(){
	
		$this->db->order_by("id", "asc");
		//$this->db->where('active', 'Y');
		$query = $this->db->get('badges');
		
		
		return $query;
	}
	
	
	function getCompleteBadgeList(){
	
		$this->db->order_by("id", "asc");
		//$this->db->where('active', 'Y');
		$query = $this->db->get('badges');
		
		
		return $query;
	}
	
	function deleteBadge($id){
		//$this->db->delete('siast_badges', array('id' => $id)); 
		$data = array('active' => 'N');
		$this->db->where('id', $id);
		$this->db->update('melonheads', $data); 
	}
	
	function addBadge($name,$file){
		
		$data = array(
   			'badge_name' => $name,
   			'badge_png' => $file 
		);
	
		$this->db->insert('melonheads', $data); 
	}
	
	
	function didUserUseBadge($user_id, $badge_id){
		$query = $this->db->get_where('badge_usage', array('user_id' => $user_id, 'badge_id' => $badge_id), 1);
		if ($query->num_rows() > 0){
			return(true);
		}
		else{
			return(false);
		}
	}
	
	function Retrive_all_records($table,$where_clause,$order_by_fld,$order_by,$limit,$offset) {
	
		if($where_clause != '')
			$this->db->where($where_clause);
        if($order_by_fld != '')
		    $this->db->order_by($order_by_fld,$order_by);
		if($limit != '' || $offset !='')
		    $this->db->limit($limit,$offset);		
				
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get(); 
		//echo $limit;
		//echo $offset;
		//echo $this->db->last_query(); exit;
		 return $query->result();
	}
	//
		function Retrive_single_records($table,$where_clause,$order_by_fld,$order_by,$limit,$offset) {
		if($where_clause != '')
			$this->db->where($where_clause);
        if($order_by_fld != '')
		    $this->db->order_by($order_by_fld,$order_by);
		if($limit != '' && $offset !='')
		    $this->db->limit($limit,$offset);		
				
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		//echo $this->db->last_query();die();  
		 return $query->row(); 
		
	}
	
	function createContest($fb_id,$name,$email,$location)
	{
		
		
				
		$data = array(
		   'fb_id' => $fb_id ,
		   'user_name' => $name ,
		   'user_email' => $email,
		   'user_location' => $location
		);
		
		
		$query = $this->db->insert('contest', $data); 
		$insertid = $this->db->insert_id();
		return $insertid;
	}
	
	function checkContest($fb_id)
	{
		
		$query = $this->db->get_where('contest', array('fb_id' => $fb_id));
		$count = $query->num_rows();
		
		//echo "-->" . $count;
		
		if($count >= 1){
			return (TRUE);
		}else{
			return (FALSE);
		}	
	}
	
	function Update_Record($row,$table,$where_clause) {
		$this->db->where($where_clause);
		
		$this->db->update($table, $row);
		//echo $this->db->last_query(); exit;
	}
	
	function Update_Record_not_in($row,$table,$where_clause) {
		$this->db->where_not_in('id',$where_clause);
		
		$this->db->update($table, $row);
		//echo $this->db->last_query(); exit;
	}
	
	function Add_Record($row,$table) {
		$str = $this->db->insert_string($table, $row);        
		$query = $this->db->query($str); 
		$insertid = $this->db->insert_id();
		return $insertid;
	
	}	// end of Add_Record
	
	function count_records($table,$where_clause,$search,$order_by_fld,$order_by,$limit,$offset) {
		if($where_clause != '')
			$this->db->where($where_clause);
		if($search != '')
			$this->db->or_like($search);	
        if($order_by_fld != '')
		    $this->db->order_by($order_by_fld,$order_by);
		if($limit != '' && $offset !='')
		    $this->db->limit($limit,$offset);			
				
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		
		 return $query->num_rows(); 
	}
	
	function Delete_records($table,$where)
	{
		$this->db->delete($table, $where); 
		//echo $this->db->last_query(); exit;
	}
	
	
}


?>