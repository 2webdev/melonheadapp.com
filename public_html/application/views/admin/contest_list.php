<div id="container"> 
  <!--Header Start-->
  
  <!-- /Header end--> 
  
  <!--content Start-->
  
  <div id="content"><form action="" name="edit_context" method="post" >
    <section class="content_inner">
    <ul class="links">
<li><a href="<?php echo base_url().'admin/contest_list/' ?>">Manage Contest</a></li><li><a href="<?php echo base_url().'admin/user_list/' ?>">User List</a></li> <li><a href="<?php echo base_url().'admin/change_password/' ?>">Change Password</a></li>  <li><a href="<?php echo base_url().'admin/logout/' ?>">Logout</a></li>
</ul>
    
     <h3 align="left">Contest List</h3>
      <br class="spacer"/>
   <a href="<?php echo base_url().'admin/add_contest/' ?>">Add New Contest</a>
   <br class="spacer"/>
    <br class="spacer"/>
	
    <table id="contest_list" border="1">
  <tr>
  
    <td height="37"> <strong># NO</strong></td>
     <td>
    <strong>Title</strong></td>
    <td>
    <strong>Description</strong></td>
    
    <td> <strong>Status</strong></td>
    <td> <strong>Edit</strong></td>
     <td> <strong>Delete</strong></td>
  </tr>
    <?php
	$i=1;
	foreach($cms_row as $key){	
	?>
   <tr>
     <td height="37"> <?php  echo $i; ?></td>
     <td><?php  echo substr($key->title,0,50); ?></td>
    <td><?php  echo substr($key->content,0,500).'..'; ?></td>
   <td> <?php
    if($key->status=='Inactive')
	{
    ?>
    <a href="<?php echo base_url().'admin/contest_list/'.$key->id.'/'.$key->status ?>" title="Make Active" ><?php echo $key->status; ?></a></td>
  	<?php
	}
	
	else
	{
	?>
        <?php echo $key->status; ?>
	<?php
	}
	?></td>
    <td align="center"><a href="<?php echo base_url().'admin/edit_contest/'.$key->id ?>" >Edit</a></td>
      <td align="center"><a href="<?php echo base_url().'admin/delete_contest/'.$key->id ?>" >Delete</a></td>
  </tr>
   <?php
   $i++;
	}
	?>
</table>

     
      </section> </form>
      </div>
</div>
<script type="text/javascript" src="<?=base_url();?>assets/javascript/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "exact",
		elements : "content1",
		theme : "advanced",
		plugins : "advimage,advlink,media,contextmenu,preview",
		theme_advanced_buttons1_add_before : "newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "preview,separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,separator",
		theme_advanced_buttons3_add_before : "",
		theme_advanced_buttons3_add : "media",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "hr[class|width|size|noshade]",		
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : true,
		apply_source_formatting : true,
		force_br_newlines : true,
		force_p_newlines : false,	
		relative_urls : true
	});

</script>