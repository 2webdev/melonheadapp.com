<div id="container">
  <div id="wrapper">
    <div class="step" id="splash" style="display:none;"> 
      
      <!-- <img src="assets/gfx/pad.jpg" alt="" /> -->
      
      <h2><strong>Step 1:</strong> &nbsp;Choose your head &amp; body.</h2>
      <div id="selector"> <img id="fpo-img" src="<?=base_url();?>assets/gfx/girl-fpo.png" alt="" /> <img id="body-img" src="<?=base_url();?>assets/gfx/body-dude.png" alt="" /> <img id="helmet-img" src="<?=base_url();?>assets/gfx/helmet-helmet.png" alt="" /> </div>
      
      <!--
			<a href="#"><img id="body-left" src="<?=base_url();?>assets/gfx/left-arrow.png" alt="" /></a>
			<a href="#"><img id="helmet-left" src="<?=base_url();?>assets/gfx/left-arrow.png" alt="" /></a>
			--> 
      
      <!--
			<a href="#"><img id="helmet-right" src="<?=base_url();?>assets/gfx/right-arrow.png" alt="" /></a>
			<a href="#"><img id="body-right" src="<?=base_url();?>assets/gfx/right-arrow.png" alt="" /></a>
			-->
      
      <div id="head-thumb-selector"> <a href="#" onclick="updateHelmet(0);return(false);"><img src="<?=base_url();?>assets/gfx/helmet-thumb.jpg" alt="" /></a> <a href="#" onclick="updateHelmet(1);return(false);"><img src="<?=base_url();?>assets/gfx/melon-thumb.jpg" alt="" /></a> <a href="#" onclick="updateHelmet(2);return(false);"><img src="<?=base_url();?>assets/gfx/mohawk-thumb.jpg" alt="" /></a> <a href="#" onclick="updateHelmet(3);return(false);"><img src="<?=base_url();?>assets/gfx/hair-thumb.jpg" alt="" /></a> <a href="#" onclick="updateHelmet(4);return(false);"><img src="<?=base_url();?>assets/gfx/gainer-thumb.jpg" alt="" /></a> </div>
      <div id="body-thumb-selector"> <a href="#"><img onclick="updateBody(0);return(false);" src="<?=base_url();?>assets/gfx/guy-thumb.jpg" alt="" /></a> <a href="#"><img onclick="updateBody(1);return(false);" src="<?=base_url();?>assets/gfx/girl-thumb.jpg" alt="" /></a> <a href="#"><img onclick="updateBody(2);return(false);" src="<?=base_url();?>assets/gfx/jersey-thumb.jpg" alt="" /></a> <a href="#"><img onclick="updateBody(3);return(false);" src="<?=base_url();?>assets/gfx/bear-thumb.jpg" alt="" /></a> </div>
      <a href="#"><img id="save-options" src="<?=base_url();?>assets/gfx/continue-btn.png" alt="" /></a> </div>
    
    <!-- !FB Login -->
    <div class="step" id="fb-login" style="display:none;">
      <h2><strong>Step 2:</strong> &nbsp;Login Using Your Facebook Account</h2>
      <p style="position:absolute;top:60px;text-align:center;width:729px;">Logging in with Facebook lets you access your photos and allows you to publish your new pic.</p>
      <div id="fb-login-box" class="inner-box"> <a href="<?=$fblu_a?>"><img style="margin-top:10px;margin-bottom:25px;margin-left:245px;" src="<?=base_url();?>assets/gfx/archive/login-with-facebook.gif" alt="Login with Facebook" /></a> </div>
      
      <!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> --> 
    </div>
    <!-- END FB Login --> 
    
    <!-- !Account Creation -->
    <div class="step" id="fb-profile" style="display:none;">
      <h2><strong>Step 3:</strong> &nbsp;Please confirm the following information.</h2>
      <div style="padding-top:25px;" id="profile-box" class="inner-box"> 
        
        <!--
					<iframe src="http://www.facebook.com/plugins/registration.php?
					             client_id=209612582424480&
					             redirect_uri=http%3A%2F%2Fmelonheadapp.com%2Fapp%2Fprofile%2F&
					             fields=name,email,location&fb_only=true"
					        scrolling="auto"
					        frameborder="no"
					        style="border:none"
					        allowTransparency="true"
					        width="660"
					        height="330">
					</iframe>
					--> 
        
      </div>
    </div>
    <!-- END Account Creation --> 
    
    <!-- !CHOOSE METHOD -->
    <div class="step" id="choose-method" style="display:none;">
      <h2><strong>Step 2:</strong> &nbsp;Choose a Facebook photo or upload your own.</h2>
      <div style="" id="type-box" class="inner-box"> <img id="fb-option" src="<?=base_url();?>assets/gfx/archive/facebook-album.gif" alt="" /> <img id="uplaod-option" src="<?=base_url();?>assets/gfx/archive/upload-photo.gif" alt="" /> </div>
      
      <!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> --> 
    </div>
    <!-- END CHOOSE METHOD --> 
    
    <!-- !Upload Photo -->
    <div class="step" id="upload-photo" style="display:none;">
      <h2><strong>Step 3:</strong> &nbsp;Upload your photo</h2>
      <?
				if(isset($up_err) && $up_err != ''){
				?>
      <p id="up-errors"> Sorry we encountered a problem uploading your photo.<br />
        <?
				switch($up_err){
					
					case '1':
						echo 'Please try again.';
					break;
					case '2':
						echo 'The photo file size was too large. Please try again.';
					break;
					case '3':
						echo 'The file doesn\'t appear to be an image. Please try again.';
					break;
					case '4':
						echo 'The photo is too short. Please try a larger photo.';
					break;
					case '5':
						echo 'The photo is too narrow. Please try a larger photo.';
					break;
					
				}
				?>
      </p>
      <?
				}
				?>
      <div id="upload-photo-box" class="inner-box">
        <form action="<?=base_url();?>app/uploadphoto/" method="post" enctype="multipart/form-data">
          <input type="file" name="user_photo" size="20">
          <input style="width:200px;" type="submit" value="Upload Photo" />
        </form>
      </div>
      <span class="info-copy">Ideally your photo will be in the jpg format and must be under 2 MB in size.</span> 
      
      <!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> --> 
    </div>
    <!-- END Upload Photo -->
    
    <?php
		if($app_state == "photoalbum"){
		?>
    <!-- !FB photo broswer -->
    <div class="step" id="fb-photos" style="display:none;">
      <h2><strong>Step 3:</strong> &nbsp;Choose your photo.</h2>
      <div id="fb-photo-box" class="inner-box">
        <?
		          	if($app_state == "photoalbum"){
		          	
			          	
						
						
						          	   	
			          	
			          	$pic_count = 0;
			          	$display_count = 0;
			          	
			          	
			          	foreach($album_photos['data'] as $photo){
							
							//28
							if( ($pic_count < ($page_num*14)+14) && ($pic_count >= ($page_num*14)) ){
							?>
        <div id="<?=$photo['id']?>" class="polaroid" style="left:<?=($display_count*117)+7?>px;">
          <div class="p-img">
            <?    
							// Supported types: thumbnail, normal, album      	
			          		echo '<a href="'.base_url().'app/crop/'.$photo['id'].'"><img onload="$(this).fadeIn(200);" style="display:none;" src="https://graph.facebook.com/'.$photo['id'].'/picture?type=album&access_token='.$access_token.'" /></a>';
			          		
			          		
			          		?>
          </div>
        </div>
        <?
			          		$display_count++;
			          		}
			          	$pic_count++;
			          	
			          	
			          	
			          	}
  						$num_pages = ceil($pic_count/14);
  						
  						
  						echo '<br style="clear:both;" /><p style="margin-top:13px;"><span style="color:#FFF;">Your Facebook Albums:</span> <br /><select name="album" id="album" style="width:175px;margin-top:5px;" onchange="albumSelect();">';
						
						foreach($albums['data'] as $album){	
							
							
							//echo $album;
							
							// set to profile if not selected..
							//if($album->name == 'Profile Pictures' && $sel_album == ""){
							// set deafult to first album found
							if($sel_album == ""){
								
								$sel_album = $album['id'];
								
								
								if($album['name'] == "Profile Pictures"){
									$sel_album = $album['id'];
								}
							}
							
							if($sel_album == $album['id']){
								echo '<option selected="selected" value="'.$album['id'].'">'.$album['name'].'</option>';
							}
							else{
								echo '<option value="'.$album['id'].'">'.$album['name'].'</option>';
							}
							
						}
						echo '</select><br /><br /></p>';
  						
  						
  		    	        //echo "-->" . $pic_count;  

					}
		          	?>
      </div>
      <br style="clear:both;" />
      <?
				//echo $num_pages;
				if($num_pages > 1){
					?>
      <div id="photo-album-nav"> Page: &nbsp;
        <?
					
					for($p=0;$p<$num_pages;$p++){
						echo "<span> | <a href='#' onclick='pageSelect(".$p.");return(false);'>".($p+1)."</a></span>";
					}
					
					?>
        |</div>
      <?
				}
				?>
    </div>
    <!-- END FB photo browser -->
    
    <?
		}
		?>
    <?php
		if($app_state == "crop"){
		?>
    <script type="text/javascript">


   $(document).ready(function() {
		
document.getElementById('cropbox').style.width = '250px';
      // Create variables (in this scope) to hold the API and image size

     var jcrop_api, boundx, boundy;
		
      $('#cropbox').Jcrop({
		onChange: saveCoords,
		onSelect: saveCoords,
		setSelect:   [ 40,40,250,250 ],
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;
      });



      function saveCoords(coords)

      {
		  
		  <?
		if($app_state_info != "crop-upload"){
		?>	
var img = document.getElementById('cropbox1'); 
//or however you get a handle to the IMG
var width = img.clientWidth;

var r = width/250;
//alert(width);
//alert(r);

<?
}
?>

        if (parseInt(coords.w) > 0)
        {
          var rx = 170 / coords.w;
          var ry = 170 / coords.h;
		<?

		if($app_state_info == "crop-upload"){

		?>

         $('#preview').css({
							width: Math.round(rx * <?=$photo_data['width']?>) + 'px',
							height: Math.round(ry * <?=$photo_data['height']?>) + 'px',
							marginLeft: '-' + Math.round(rx * coords.x) + 'px',
							marginTop: '-' + Math.round(ry * coords.y) + 'px'
						});
		<?
		}
		else{
		?>

		$('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
            marginTop: '-' + Math.round(ry * coords.y) + 'px'

          });

		<?
		}
		?>
	//alert(r);
	<?
		if($app_state_info == "crop-upload"){
		?>
		  var c1=parseInt(coords.x);
		  var c2=parseInt(coords.y);
		  var c3=parseInt(coords.x2);
		  var c4=parseInt(coords.y2);
		  var c5=parseInt(coords.w);
		  var c6=parseInt(coords.h);
		 <?php
		}
		 else
		 {
		 ?> 
		  var c1=parseInt(coords.x*r);
		  var c2=parseInt(coords.y*r);
		  var c3=parseInt(coords.x2*r);
		  var c4=parseInt(coords.y2*r);
		  var c5=parseInt(coords.w*r);
		  var c6=parseInt(coords.h*r);
		<?php
				 }
		?>
			$('#xx').val(c1);
			$('#yy').val(c2);
			$('#x2').val(c3);
			$('#y2').val(c4);
			$('#w').val(c5);
			$('#h').val(c6);
        }
      };
	  
    });
	
	function showCrop(){
		$('.data').css('visibility','visible');
	}
  </script>
    <style type="text/css">
		.footnote{
			top:642px;
		}
		</style>
    
    <!-- !CROP PHOTO -->
    <?
		echo "<!--";
		print_r($photo_data);
		echo "-->";
		?>
    <div class="step" id="crop-photo" style="display:none;">
      <h2><strong>Step 4:</strong> &nbsp;Crop your photo.</h2>
      
        <div class="cropImg">
      
          <?
				?>
          <?
$width = 200;
/* Get the image info */
				if($app_state_info == "crop-upload"){
				echo '<img  onload="showCrop();" id="cropbox" src="'.$photo_data['websource'].'" />';
				}
				else{
				// echo '<img onload="showCrop();" id="cropbox" src="https://graph.facebook.com/'.$photo_id.'/picture?type=normal&access_token='.$access_token.'" />';
				echo '<img  onload="showCrop();" id="cropbox" src="'.$photo_data['images'][0]['source'].'" />';
				echo '<div class="hideImg" style="visibility:hidden;"><img   id="cropbox1" src="'.$photo_data['images'][0]['source'].'" /></div>';
				}
				?>
        </div>
        
        <div class="previewFrame">
          <div id="prevPann">
            <div id="preview-pane" style="width:133px;height:184px;overflow:hidden; margin-left:-5px;">
              <? 
						if($app_state_info == "crop-upload"){
						?>
              <img id="preview" src="<?=$photo_data['websource']?>" />
              <?
						}
						else{
						?>
              <!-- <img id="preview" src="https://graph.facebook.com/<?=$photo_id?>/picture?type=normal&access_token=<?=$access_token?>" /> --> 
              <img id="preview" src="<?=$photo_data['images'][0]['source']?>" />
              <?
						}
						?>
            </div>
          </div>
          <img id="the-body" src="" />
          <div class="helmet"> <img id="the-helmet" src="" /> </div>
          <script type="text/javascript">
            $(document).ready(function() {
                $('#the-helmet').attr('src','<?=base_url();?>assets/gfx/'+helmetImages[<?=$hid?>]);
                $('#the-body').attr('src','<?=base_url();?>assets/gfx/'+bodyImages[<?=$bid?>]);
            });
        </script> 
          
          <!--<img class="helmet" src="http://melonheadapp.com/assets/gfx/helmet-helmet.png" alt="" />--> 
          
        </div>

      <form id="crop-form" name="crop-form" action="<?=base_url()?>app/savephoto/" method="post">
        <input type="hidden" id="xx" name="xx" value="" />
        <input type="hidden" id="yy" name="yy" value="" />
        <input type="hidden" id="x2" name="x2" value="" />
        <input type="hidden" id="y2" name="y2" value="" />
        <input type="hidden" id="w" name="w" value="" />
        <input type="hidden" id="h" name="h" value="" />
        <input type="hidden" id="bx" name="bx" value="96" />
        <input type="hidden" id="by" name="by" value="96" />
        <input type="hidden" id="b-color" name="b_color" value="<?=$color?>" />
        <?
						if($app_state_info == "crop-upload"){
						?>
        <input type="hidden" id="p-src" name="p_src" value="<?=$photo_data['source']?>" />
        <?
						}
						else{
						?>
        <input type="hidden" id="p-src" name="p_src" value="<?=$photo_data['images'][0]['source']?>" />
        <?
						}
						?>
        <input type="hidden" id="p-id" name="p_id" value="<?=$photo_data['id']?>" />
        <input id="save-photo-btn" type="image" src="<?=base_url();?>assets/gfx/save-photo-btn.png" />
        <!-- <input type="submit" /> -->
      </form>
    </div>
    <!-- END CROP PHTO -->
    <?php
		}
		?>
    <?php
		if($app_state == "publish"){
		?>
    <style type="text/css">
		.footnote{
			top:617px;
		}
		</style>
    
    <!-- !Publish Photo -->
    <div class="step" id="photo-publish" style="display:none;">
      <h2><strong>Step 4:</strong> &nbsp;Share your photo.</h2>
      <div id="photo-publish-box" class="inner-box" style="padding-top:0px;"> <img style="margin-top:3px;" src="<?=base_url()?>assets/user_photos/<?=$photo_id?>?c=<?=rand();?>" alt="Your New Photo" />
        <div id="publish-options"> <span class="info-copy-lrg"> <b>Your photo has been saved to a new album called "melon head app".</b> </span>
          <p> <br>
            &raquo; <a href="<?=$the_link?>&makeprofile=1" target="_blank">Click here to make this your Facebook profile picture.</a> <br>
          </p>
          
          <!--<p>
							<br>
							
							&raquo; <a href="<?=base_url()?>twitter/index.php?authorize=1" title="Make this your profile picture on Twitter" id="twitlink" name="windowX">Click here to make this your Twitter profile picture.</a>
							
	 
							<script type="text/javascript"> 
								$('#twitlink').popupWindow({ 
									height:900, 
									width:515,
									resizable:1,
									scrollbars:1, 
									centerBrowser:1 
								}); 
							</script>

							<br>
						</p>-->
          
          <p> <br />
            &raquo; <a href="#" onclick="FB.ui({method: 'apprequests', message: 'melon head app  |  Get your game gear on. www.melonheadapp.com powered by SIAST.', data: 'forward'});return(false);">Click here to invite your friends to use the melon head app.</a> </p>
          <p> <br />
            <b>Thanks for getting your game gear on and creating an online sea of green.</b> </p>
          <br />
          <p> <a href="<?=base_url()?>app/contest_user/">Enter Contest</a></p>
          <div id="share-box">
            <p> <br />
              <b>Share your shout out to the Riders here:</b> <br />
              <textarea name="shoutout" id="shout-out" cols="30" rows="10"></textarea>
            </p>
            <script type="text/javascript">
								
								$(document).ready(function() {
									//console.log('cc');
									$("#shout-out").counter();
								});
							</script> 
            <a href="#" onclick="pF();return(false);"><img src="<?=base_url()?>assets/gfx/share-to-fb.png" alt="" /></a> </div>
          <div id="success-box" style="display:none;">
            <p> <br />
              <br />
              <b>Thanks for Sharing!</b> </p>
          </div>
        </div>
        
        <!--
						<p style="font-size:13px;" id="social-links">
						<br />
						Stay in touch with us. Check us out on:&nbsp;&nbsp;&nbsp;&nbsp;
					
						
						<a href="http://www.facebook.com/SIAST/" target="_blank"><img src="<?=base_url();?>assets/gfx/facebook-link.png" alt="Facebook" /></a>
						<a href="http://www.twitter.com/SIAST/" target="_blank"><img src="<?=base_url();?>assets/gfx/twitter-link.png" alt="Twitter" /></a>
						<a href="http://www.youtube.com/SIASTtv/" target="_blank"><img src="<?=base_url();?>assets/gfx/youtube-link.png" alt="Youtube" /></a>
						</p>
					--> 
        
      </div>
      
      <!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> --> 
    </div>
    <!-- END Publish Photo -->
    <?php
		}
		?>
        <br />
    <p class="footnote">SIAST is a proud sponsor of the <br />
      Saskatchewan Roughrider Football Club</p>
  </div>
</div>
