<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<title>melon head app</title>
		
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
		<meta http-equiv="X-UA-Compatible" content="chrome=1">

		<meta property="og:title" content="melon head app"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://melonheadapp.com/"/>
		<meta property="og:image" content="http://melonheadapp.com/assets/gfx/fb-icon.png"/>
		<meta property="og:site_name" content="melon head app"/>
		<meta property="fb:admins" content="100000185377198"/>
		<meta property="fb:app_id" content="209612582424480"/>
		<meta property="og:description" content="Get your game gear on. www.melonheadapp.com powered by SIAST"/>
		
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url();?>assets/gfx/favicon.ico" />
		
		<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/melons.css" media="all" />
		<link href="http://fonts.googleapis.com/css?family=Bevan&v2" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/css/cupertino/jquery-ui-1.8.12.custom.css" />	
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/javascript/jcrop/css/jquery.Jcrop.css" />
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>assets/javascript/jquery-ui-1.8.12.custom.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>assets/javascript/jcrop/js/jquery.Jcrop.min.js"></script>
		
		<script type="text/javascript">
			if (top.location!= self.location) {
				top.location = self.location.href;
			}
		</script>
	
	</head>
	<body><div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#appId=209612582424480&xfbml=1"></script>
	
	<!--
<div id="header">
		<a href="<?=base_url()?>"><img src="<?=base_url();?>assets/gfx/header.png" alt="" /></a>
	</div>
-->
