<div id="splash" style="display:none;">
	<div id="splash-click-through">
	</div>
	
	<!-- <img id="splash-photo" src="<?=base_url();?>assets/gfx/splash-photo.jpg" /> -->
	<img id="splash-photo-a" src="<?=base_url();?>assets/gfx/splash-a.jpg" alt="Wear it and Win - Show your support. Click here to get started." />
	<a href="http://www.gosiast.com" target="_blank"><img id="splash-photo-b" src="<?=base_url();?>assets/gfx/splash-b.jpg" alt="Visit goSIAST.com" /></a>
	
	<div id="splash-social-links">
		<a href="http://www.facebook.com/SIAST/" target="_blank"><img id="s-fb-l" src="<?=base_url();?>assets/gfx/facebook-s-link.png" alt="SIAST on Facebook" /></a>
		<a href="http://www.twitter.com/SIAST/" target="_blank"><img id="s-tw-l" src="<?=base_url();?>assets/gfx/twitter-s-link.png" alt="SIAST on  Twitter" /></a>
		<a href="http://www.youtube.com/SIASTtv/" target="_blank"><img id="s-yt-l" src="<?=base_url();?>assets/gfx/youtube-s-link.png" alt="SIAST on  Youtube" /></a>
	</div>
	
	
	
</div>

<div id="app-wrapper" style="display:none;">
	
	<div id="app">
	
			<div id="app-head">
				<a href="<?=base_url();?>"><img id="siast-badge" src="<?=base_url();?>assets/gfx/siast-badges.gif" alt="SIAST Badges" /></a>
				<img id="wear-it-and-win" src="<?=base_url();?>assets/gfx/wear-it-and-win.gif" alt="" />
			</div>
			
			<!-- START APP BODY -->
			<div id="app-body">
				
				
				<!-- !CHOOSE BADGE -->
				<div class="step" id="choose-badge" style="display:none;">
					
					<h2><strong>Step 1:</strong> &nbsp;Choose your Badge</h2>
					
						<div id="badge-box" class="inner-box">
						
							<img src="<?=base_url();?>assets/gfx/box-top.png" alt="" width="678" />
							<img src="<?=base_url();?>assets/gfx/box-middle.png" alt="" width="678" height="90" />
							<img src="<?=base_url();?>assets/gfx/box-bottom.png" alt="" width="678" />
							
							<div id="badges">
								<?
								if($app_state == "initial"){
									foreach ($badges->result() as $badge){
									    //echo $row->title;
									    ?><img id="badge-<?=$badge->id?>" onclick="saveBadgeId(<?=$badge->id?>);" class="badge" src="<?=base_url();?>badges/<?=$badge->badge_png?>.png" alt="Badge" /><?
									}
								}
								?>
							</div>
							
						</div>
						
						<img src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">This is the graphic you'll overlay on top of your photo. You'll choose which photo to use later.</span>
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END CHOOSE BADGE -->
				
				
				
				<!-- !FB Login -->
				<div class="step" id="fb-login" style="display:none;">
					
					<h2><strong>Step 2:</strong> &nbsp;Register with SIAST using your Facebook Account</h2>
					
						<div id="fb-login-box" class="inner-box">
							
							<a href="<?=$fblu_a?>"><img style="margin-top:5px;margin-bottom:25px;" src="<?=base_url();?>assets/gfx/login-with-facebook.gif" alt="Login with Facebook" /></a>
							
						</div>
						
						
						
						
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">We'll ask for your name and email address, and use that when selecting a winner.</span>
						
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END FB Login -->

				
				
				
				<!-- !CHOOSE METHOD -->
				<div class="step" id="choose-method" style="display:none;">
					
					<h2><strong>Step 2:</strong> &nbsp;Where is your photo?</h2>
					
						<div style="" id="type-box" class="inner-box">
							<img id="fb-option" src="<?=base_url();?>assets/gfx/facebook-album.gif" alt="" />								
							<img id="uplaod-option" src="<?=base_url();?>assets/gfx/upload-photo.gif" alt="" />
						</div>
						
						
						
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">Choose a photo you have on Facebook or upload a new photo.</span>
						
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END CHOOSE METHOD -->
				
				
				
								
				
				<!-- !Account Creation -->
				<div class="step" id="fb-profile" style="display:none;">
					
					<h2><strong>Step 3:</strong> &nbsp;Please confirm the following information.</h2>
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">Click the X next to your name if you need to correct any information provided by Facebook..</span>
						
						<div style="padding-top:25px;" id="profile-box" class="inner-box">
							
							<iframe src="http://www.facebook.com/plugins/registration.php?
							             client_id=161303247267213&
							             redirect_uri=http%3A%2F%2Fsiastbadges.com%2Fapp%2Fprofile%2F&
							             fields=name,email,location&fb_only=true"
							        scrolling="auto"
							        frameborder="no"
							        style="border:none"
							        allowTransparency="true"
							        width="660"
							        height="330">
							</iframe>
							
						</div>
						
						
						
						<!--
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">You can choose a photo you have on Facebook or upload a new photo.</span>
						-->
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END Account Creation -->

				
				
				
				<!-- !Upload Photo -->
				<div class="step" id="upload-photo" style="display:none;">
					
					<h2><strong>Step 2:</strong> &nbsp;Choose your Photo</h2>
						<?
						if(isset($up_err) && $up_err != ''){
						?>
						<p id="up-errors">
						Sorry we encountered a problem uploading your photo.<br />
						
						<?
						switch($up_err){
							
							case '1':
								echo 'Please try again.';
							break;
							case '2':
								echo 'The photo file size was too large. Please try again.';
							break;
							case '3':
								echo 'The file doesn\'t appear to be an image. Please try again.';
							break;
							case '4':
								echo 'The photo is too short. Please try a larger photo.';
							break;
							case '5':
								echo 'The photo is too narrow. Please try a larger photo.';
							break;
							
						}
						?>
						
						</p>
						<?
						}
						?>
						
						<div id="upload-photo-box" class="inner-box">
							
							<form action="<?=base_url();?>app/uploadphoto/" method="post" enctype="multipart/form-data">
							<input type="file" name="user_photo" size="20"> 
							<input type="submit" value="Upload Photo" />
							</form>
							
						</div>
						
						
						
						
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">Ideally your photo will be in the jpg format, and must be under 2 MB in size.</span>
						
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END Upload Photo -->

				
				
				
				
				<?php
				if($app_state == "photoalbum"){
				?>
				<!-- !FB photo broswer -->
				<div class="step" id="fb-photos" style="display:none;">
					
					<h2><strong>Step 3:</strong> &nbsp;Choose your Photo</h2>
					
						<div id="fb-photo-box" class="inner-box">
							
							<?
				          	if($app_state == "photoalbum"){
				          	
					          	echo '<p>Your FB Albums: <br /><select name="album" id="album" style="width:280px;" onchange="albumSelect();">';
								
								foreach($albums['data'] as $album){	
									
									
									//echo $album;
									
									// set to profile if not selected..
									//if($album->name == 'Profile Pictures' && $sel_album == ""){
									// set deafult to first album found
									if($sel_album == ""){
										
										$sel_album = $album['id'];
										
										
										if($album['name'] == "Profile Pictures"){
											$sel_album = $album['id'];
										}
									}
									
									if($sel_album == $album['id']){
										echo '<option selected="selected" value="'.$album['id'].'">'.$album['name'].'</option>';
									}
									else{
										echo '<option value="'.$album['id'].'">'.$album['name'].'</option>';
									}
									
								}
								echo '</select><br /><br /></p>';
								
								
								          	   	
					          	
					          	$pic_count = 0;
					          	$display_count = 0;
					          	
					          	
					          	foreach($album_photos['data'] as $photo){
									
									//28
									if( ($pic_count < ($page_num*14)+14) && ($pic_count >= ($page_num*14)) ){
									?>
									<div id="<?=$photo['id']?>" class="polaroid" style="left:<?=($display_count*117)+7?>px;">
									<div class="p-img">
									<?    
									// Supported types: thumbnail, normal, album      	
					          		echo '<a href="'.base_url().'app/crop/'.$photo['id'].'"><img onload="$(this).fadeIn(200);" style="display:none;" src="https://graph.facebook.com/'.$photo['id'].'/picture?type=album&access_token='.$access_token.'" /></a>';
					          		
					          		
					          		?>
					          		</div>
					          		</div>
					          		<?
					          		$display_count++;
					          		}
					          	$pic_count++;
					          	
					          	
					          	
					          	}
          						$num_pages = ceil($pic_count/14);
          		    	        //echo "-->" . $pic_count;  

							}
				          	?>
							
						</div>
						
						
						<br style="clear:both;" />
						
						<?
						//echo $num_pages;
						if($num_pages > 1){
							?><div id="photo-album-nav"> Page: &nbsp;<?
							
							for($p=0;$p<$num_pages;$p++){
								echo "<span> | <a href='#' onclick='pageSelect(".$p.");return(false);'>".($p+1)."</a></span>";
							}
							
							?> |</div><?
						}
						?>
						
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">Photos with a lot of open space tend to work best.</span>
						
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END FB photo browser -->
				
				<?
				}
				?>


				
				<?php
				if($app_state == "crop"){
				?>
				
				
				<script type="text/javascript">
					$(document).ready(function() {
				
						$('#cropbox').Jcrop({
							boxWidth: 250, 
							boxHeight: 250,
							allowSelect: false,
							onSelect: saveCoords,
				            onChange: saveCoords,
				            bgColor:     'white',
				            bgOpacity:   .3,
				            setSelect:   [ 40,40,250,250 ],
				            aspectRatio: 1
				        });
				        
				        $( "#the-badge" ).draggable({ containment: "parent" });
				        
				        $( "#the-badge" ).bind( "dragstop", function(event, ui) {
  							
  							$('#bx').val(ui.position.left);
							$('#by').val(ui.position.top);
  							
						});
				
					});
					
					
					function saveCoords(coords){
						
						var rx = 200 / coords.w;
						var ry = 200 / coords.h;
					
						$('#preview').css({
							width: Math.round(rx * <?=$photo_data['width']?>) + 'px',
							height: Math.round(ry * <?=$photo_data['height']?>) + 'px',
							marginLeft: '-' + Math.round(rx * coords.x) + 'px',
							marginTop: '-' + Math.round(ry * coords.y) + 'px'
						});
			
						
						$('#xx').val(coords.x);
						$('#yy').val(coords.y);
						$('#x2').val(coords.x2);
						$('#y2').val(coords.y2);
						$('#w').val(coords.w);
						$('#h').val(coords.h);
				
					}
				
					
					
					function showCrop(){
						$('.data').css('visibility','visible');
					}

				</script>
									
				
				<!-- !CROP PHOTO -->
				<div class="step" id="crop-photo" style="display:none;">
					
					<h2><strong>Step 4:</strong> &nbsp;Crop your Photo, and place badge.</h2>
						
						<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:inline;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy">Crop your photo as needed on the left. Drag &amp; drop your badge to the best position on the right.</span>
						
						<div class="data" style="visibility:hidden;">
						<?
						
						?>
						<?
						if($app_state_info == "crop-upload"){
						
						echo '<img onload="showCrop();" id="cropbox" src="'.$photo_data['websource'].'" />';
						
						}
						else{
						
						echo '<img onload="showCrop();" id="cropbox" src="https://graph.facebook.com/'.$photo_id.'/picture?type=normal&access_token='.$access_token.'" />';
						
						}
						?>
						</div>
						
						
						<div id="ppane">
							
							<div id="preview-pane">
								<?
								if($app_state_info == "crop-upload"){
								?>
								<img id="preview" src="<?=$photo_data['websource']?>" />
								<?
								}
								else{
								?>
								<img id="preview" src="https://graph.facebook.com/<?=$photo_id?>/picture?type=normal&access_token=<?=$access_token?>" />
								<?
								}
								?>
								
						
							</div>
						
							<img id="the-badge" src="<?=base_url();?>badges/<?=$badge_png?>.png" />
		
						</div>
						
						
				
						
						<form id="crop-form" name="crop-form" action="<?=base_url()?>app/savephoto/" method="post">
							
							<input type="hidden" id="xx" name="xx" value="" />
							<input type="hidden" id="yy" name="yy" value="" />
							<input type="hidden" id="x2" name="x2" value="" />
							<input type="hidden" id="y2" name="y2" value="" />
							<input type="hidden" id="w" name="w" value="" />
							<input type="hidden" id="h" name="h" value="" />
							
							<input type="hidden" id="bx" name="bx" value="96" />
							<input type="hidden" id="by" name="by" value="96" />
							
						  
							<input type="hidden" id="b-color" name="b_color" value="<?=$color?>" />
							
							<input type="hidden" id="p-src" name="p_src" value="<?=$photo_data['source']?>" />
							<input type="hidden" id="p-id" name="p_id" value="<?=$photo_data['id']?>" />
							
							<input id="save-photo-btn" type="image" src="<?=base_url();?>assets/gfx/save-photo.png" />
							<!-- <input type="submit" /> -->
						</form>
						
						
						
				</div>
				<!-- END CROP PHTO -->
				<?php
				}
				?>

				
				
				
				
				
				<?php
				if($app_state == "publish"){
				?>
				<!-- !Publish Photo -->
				<div class="step" id="photo-publish" style="display:none;">
					
					<h2><strong>Step 4:</strong> &nbsp;Share your Photo</h2>
					
						<div id="photo-publish-box" class="inner-box" style="padding-top:19px;">
							
							<img style="margin-top:3px;border:1px solid #000;" src="<?=base_url()?>assets/user_photos/<?=$photo_id?>" alt="Your New Photo" />
							
							<div id="publish-options">
								<!--
<a id="save-to-facebook-btn" href="#" onclick="savetofb();shareFB();return(false);"><img src="<?=base_url();?>assets/gfx/save-to-facebook.png" alt="Save to Facebook" /></a>
								<p style="display:none;" id="photo-saved-msg">Photo saved. <a href="">Click here to view your photo on Facebook</a></p>
-->
								<!--
								<a href="#"><img src="<?=base_url();?>assets/gfx/post-to-my-wall.png" alt="" /></a>
								-->
								<!-- <a href="#"><img src="<?=base_url();?>assets/gfx/email.png" alt="Send Email" /></a> -->
								
								<img id="info-icon" src="<?=base_url();?>assets/gfx/info-icon.png" style="display:none;position:relative;top:4px;margin-right:5px;" alt="" /><span class="info-copy-lrg">
								
								Your photo has been saved to Facebook <br />under a new album called "SIASTBadges".
								<br /><br />
								But you're not done yet! <br /><a href="<?=$the_link?>&makeprofile=1" target="_blank">Click here to make this your profile picture <br />and be entered into the draw</a>
								</span>
								
								
								<!--
<p>
								<a href="http://www.facebook.com/profile.php?id=<?=$uid?>&v=photos" target="_blank">View your new photo on Facebook</a>
								</p>
								
-->
								<p>
								<br />
								You may also want to:
								<br />
								<a href="#" onclick="shareFB();return(false);">Share your photo on your wall</a>
								</p>
								<p>
								<a href="#" onclick="FB.ui({method: 'apprequests', message: 'Students, grads, employees and those of you who just love SIAST: click here to choose your own SIASTBadge. Be entered to win an iPad 2 or 1 of 20 Walmart gift cards.', data: 'forward'});return(false);">Invite friends to use SIASTBadges</a>
								</p>
								
								
								
							</div>
							
							<p style="font-size:13px;" id="social-links">
								<br />
								Stay in touch with us. Check us out on:&nbsp;&nbsp;&nbsp;&nbsp;
							
								
								<a href="http://www.facebook.com/SIAST/" target="_blank"><img src="<?=base_url();?>assets/gfx/facebook-link.png" alt="Facebook" /></a>
								<a href="http://www.twitter.com/SIAST/" target="_blank"><img src="<?=base_url();?>assets/gfx/twitter-link.png" alt="Twitter" /></a>
								<a href="http://www.youtube.com/SIASTtv/" target="_blank"><img src="<?=base_url();?>assets/gfx/youtube-link.png" alt="Youtube" /></a>
								</p>
							
						</div>
						
						
						
						
						
						
						<!-- <a href="#"><img class="next-btn hover" src="<?=base_url();?>assets/gfx/next-step.png" alt="Next Step" /></a> -->
				</div>
				<!-- END Publish Photo -->
				<?php
				}
				?>


				
				
				
				
				
				
	
				
			</div>
			<!-- END APP BODY -->
			
			
			
	</div>
</div>


<div id="footer">
<span>Saskatchewan residents only  |  No purchase necessary to win  <br />Visit <a href="http://www.facebook.com/SIAST" target="_blank">www.facebook.com/SIAST</a> for full contest rules  | <a href="http://www.goSIAST.com" target="_blank">goSIAST.com</a></span>
<div id="fblike">
<fb:like href="http://www.facebook.com/SIAST" send="true" layout="button_count" width="450" show_faces="false" font="lucida grande"></fb:like>
</div>
</div>