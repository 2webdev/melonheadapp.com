<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
	
		<title>SIASTBadges</title>
		
		
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		
		<link rel="shortcut icon" href="<?=base_url();?>assets/gfx/favicon.ico" />
		
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Nunito">
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/css/global.css" />
		<!--[if lte IE 7]>
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/css/ie.css" />	
		<![endif]-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/css/cupertino/jquery-ui-1.8.12.custom.css" />	
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url();?>assets/javascript/jcrop/css/jquery.Jcrop.css" />
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>assets/javascript/jquery-ui-1.8.12.custom.min.js"></script>
		<script type="text/javascript" src="<?=base_url();?>assets/javascript/jcrop/js/jquery.Jcrop.min.js"></script>
		
		<script type="text/javascript">
			
			if (top.location!= self.location) {
				top.location = self.location.href;
			}
			
			$(document).ready(function() {
				
				
				
				$('#splash-photo-a').click(function(){
					$('#splash').hide(0);
					$('#choose-badge').show(0);
					
					if ( $.browser.msie ) {
						$('#app-wrapper').show(0);
					}
					else{
						$('#app-wrapper').fadeIn(150);
					}
					
				});
				
				
				
				$(".hover").hover(function() {
					$(this).attr("src", $(this).attr("src").split(".").join("-hover."));
				}, function() {
					$(this).attr("src", $(this).attr("src").split("-hover.").join("."));
				});
				
				
				
								
				
				<?
				if($fbconnection == "no"){
				?>
				$('.badge').click(function(){
					
					$('#choose-badge').hide(0);
					
					if ( $.browser.msie ) {
						$('#fb-login').show(0);
					}
					else{
						$('#fb-login').fadeIn(150);
					}
					
				});
				<?
				}
				else{
				?>
				$('.badge').click(function(){
					$('#choose-badge').hide(0);
					
					
					if ( $.browser.msie ) {
						$('#choose-method').show(0);
					}
					else{
						$('#choose-method').fadeIn(150);
					}
					
				});
				<?
				}
				?>
				
				
				
				
				
				
				
				$('#fb-option').click(function(){
					var url = '<?=base_url()?>app/photoalbum'
					window.location = url;
				});
				
				$('#uplaod-option').click(function(){
					//var url = '<?=base_url()?>app/photoalbum'
					//window.location = url;
					$('#choose-method').hide(0);
					
					
					if ( $.browser.msie ) {
						$('#upload-photo').show(0);
					}
					else{
						$('#upload-photo').fadeIn(150);
					}
					
				});
				
				
				
				//alert('<?=$app_state?>');
				
				var app_state = '<?=$app_state?>';
				
				switch(app_state){
					
					case 'initial':
						$('#splash').show(0);
					break;
					
					case 'choosemethod':
						$('#choose-method').show(0);
						$('#app-wrapper').show(0);
					break;
					
					case 'choosephoto':
						$('#upload-photo').show(0);
						$('#app-wrapper').show(0);
					break;
					
					case 'profile':
						$('#fb-profile').show(0);
						$('#app-wrapper').show(0);
					break;
					
					case 'photoalbum':
						$('#fb-photos').show(0);
						$('#app-wrapper').show(0);
					break;
					
					case 'crop':
						$('#crop-photo').show(0);
						$('#app-wrapper').show(0);
					break;
					
					case 'publish':
						$('#photo-publish').show(0);
						$('#app-wrapper').show(0);
						shareFB();
					break;
					
					default:
					break;
				}
				
				
			});
			
			
			
			function albumSelect(){
		
				var url = '<?=base_url()?>app/photoalbum'
				
				window.location = url + "/" + $('#album :selected').val() + "/0/";
				
			}
			
			function pageSelect(pnum){
		
				var url = '<?=base_url()?>app/photoalbum'
				
				window.location = url + "/" + $('#album :selected').val() + "/" + pnum + "/";
				
			}

			function saveBadgeId(id){
				//console.log('saving..');
				$.ajax({
				  url: "<?=base_url();?>app/saveBadgeId/"+id+"/",
				  cache: false,
				  success: function(resp){
				  	//console.log('done ' + resp);  
				  	//alert('done' + resp);
				  }
				});
			}
				
			function chooseBadge(){
			}
			
			
			function savetofb(){
				
				
				$.ajax({
				  url: "<?=base_url();?>app/savetofacebook/",
				  cache: false,
				  success: function(resp){
				  	//console.log('done ' + resp); 
				  	$('#save-to-facebook-btn').hide(0); 
				  	$('#photo-saved-msg').show(0);
				  	
				  }
				});
			}
			
			
			
			
			
			<?php
				if($app_state == 'publish'){
			?>
			
				
			function shareFB(){
				<?
				$config['appId'] = $this->config->item('appId');
				?>
				FB.init({appId: '<?=$config['appId']?>', status: true, cookie: true, xfbml: true, display: 'dialog'});
				
				FB.ui(
				  {
				   	  method: 'feed',
				   	  display: 'dialog',		   
				      name: 'SIASTBadges',
				      picture: '<?=base_url()?>assets/user_photos/<?=$photo_id?>',
				      link: 'http://siastbadges.com',
				      caption: 'Students, grads, employees and those of you who just love SIAST: click here to choose your own SIASTBadge. It\'s not too late to apply for SIAST certificate, diploma and continuing education classes for fall 2011! Visit www.gosiast.com/choices.shtml to find programs with seats still available.',
				      href: 'http://siastbadges.com',
				      description: ' ',
				      message: 'I just added a SIASTBadge to my Facebook profile and have been entered to win an iPad 2 or 1 of 20 Walmart gift cards. Show your support for SIAST and enter to win!'
				   
				    
				  },
				  function(response) {
				    if (response && response.post_id) {
				      //alert('Post was published.');
				      
				    } else {
				      //alert('Post was not published.');
				      
				    }
				  }
				);
			}

			<?
				}
			?>
			
		</script>
		
	</head>
	<body><div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
