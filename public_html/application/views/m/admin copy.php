<div id="app-wrapper">
	
	<style type="text/css">
		
		#badge-container{
			width:690px;
			height:150px;
			overflow-x:hidden;
			/* border: 1px solid red; */
			position: relative;
			left:-10px;
		}
		
		.badge-wrapper{
			width:650px;
			position: relative;
			width:200px;
			height:50px;
			/* border: 1px solid red; */
		}
		
		.badgeedit{
			width:200px;
			height:50px;
			border:2px solid #EEE;
			background-color: #FFF;
			
			float:left;
			margin:7px;
			padding:3px;
		}
		
		.badgeedit span{
			font-size:11px;
			position: absolute;
			top:5px;
			left:60px;
		}
		
		.badgeedit a{
			font-size:11px;
			position: absolute;
			top:25px;
			left:60px;
		}
		
		.badgeedit img{
			display: inline;
		}
		
		
		#add-badge{
			clear: both;
			padding-top:20px;
			
		}
		
		
		#add-form{
			width:645px;
			height:100px;
			border:2px solid #EEE;
			background-color: #FFF;
			margin-top:5px;
			
			padding:3px;
			
			font-size:11px;
			padding-left:5px;
		}
		
		
		#b-info{
			position: absolute;
			left:410px;
			top:330px;
			border:1px dashed #4e4e4d;
			background-color: #fdf3ad;
			font-size:11px;
			padding:15px;
		}
		
	</style>
	
	<div id="app">
	
			<div id="app-head">
				<a href="<?=base_url();?>"><img id="siast-badge" src="<?=base_url();?>assets/gfx/siast-badges.gif" alt="SIAST Badges" /></a>
				<img id="wear-it-and-win" src="<?=base_url();?>assets/gfx/wear-it-and-win.gif" alt="" />
			</div>
			
			<!-- START APP BODY -->
			<div id="app-body">
			
				<div class="step">	
				<h2><strong>SIASTBadges Administration</strong></h2>
				
				<p><br /><?=$total_users?> users. <?=$total_badges?> badges created. &nbsp; - &nbsp;<a href="<?=base_url()?>app/manage/export/" target="_blank">Export Excel list</a></p>
				
				
				
				<p style="padding-top:25px;"><strong>Badge Management</strong></p>
				
				<div id="badge-container">
				
				<?
				foreach($badges->result() as $badge){
					?>
					<div class="badgeedit"><div class="badge-wrapper">
					
					<img width="48" src="<?=base_url();?>badges/<?=$badge->badge_png?>.png" alt="badge" />
					<span><?=$badge->badge_name;?></span>
					
					<a onclick="return confirm('Are you sure?');" href="<?=base_url();?>app/manage/delete/<?=$badge->id?>">Delete Badge</a>
					
					</div></div>
					<?
					//echo $badge->badge_name;
				}
				?>
				
				</div>
				
				<div id="add-badge">
				<p><strong>Add Badge:</strong> <span style="font-size:11px;">Note that you can only have the first 5 available at any one time.</span></p>
				
				<div id="add-form">
				
					<form action="<?=base_url();?>app/manage/upload/" method="post" enctype="multipart/form-data">
						
						<table style="margin-top:5px;">
							<tr>
								<td>Badge Name:</td>
								<td><input type="text" name="bname" size="20" /></td>
							</tr>
							<tr>
								<td>Badge File:</td>
								<td><input type="file" name="bimage" size="20"></td>
							</tr>
						</table>
						<br />
						<input type="submit" value="Upload New SIASTBadge" />
						
					</form>
					
				</div>
				
				</div>
				
				<div id="b-info">
					Badges need to be in a specific format:<br />
					- 86 x 86 pixels<br />
					- PNG File Type 
				</div>
				
				
				</div>	
				
				
			</div>
			<!-- END APP BODY -->
			
			
			
	</div>
</div>


<div id="footer">
<span>Saskatchewan residents only  |  No purchase necessary to win  <br />Visit <a href="http://www.facebook.com/SIAST" target="_blank">www.facebook.com/SIAST</a> for full contest rules  | <a href="http://www.goSIAST.com" target="_blank">goSIAST.com</a></span>
<div id="fblike">
<fb:like href="http://www.facebook.com/SIAST" send="true" layout="button_count" width="450" show_faces="false" font="lucida grande"></fb:like>
</div>
</div>