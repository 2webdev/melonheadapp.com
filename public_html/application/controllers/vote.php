<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vote extends CI_Controller {
	
	private $uid = null;
	
	function __construct()
	{
		parent::__construct();
	}

	function index(){
		
		// FIND OUT IF USER IS CONNECTED
		$data['fbconnection'] = $this->fbconnect(TRUE);
		// GET LOGIN URL
		$data['fbloginurl'] = $this->fbloginUrl('home');
		
		// IF CONNECTED FIND OUT IF VOTED
		$data['voted_for'] = "noone";
		if($data['fbconnection'] == 'yes'){
			
			$data['fbuid'] = $this->uid;
			
			$this->load->model('contestants');
			$data['voted_for'] = $this->contestants->getVote($data['fbuid']);
			
		}

		
		$data['nav'] = "home";
		$this->load->view('top', $data);
		
		$this->load->model('contestants');
		
		$data['contestants'] = $this->contestants->currentContestants();
		
		$this->load->view('vote',$data);
		
		
		$data['userinfo'] = $this->contestants->contestantData();
		$data['userinfo']['namelink'] = str_replace(' ','_',$data['userinfo']['fname']);
		
		$data['round'] = "";
		$data['userinfo']['link'] = $data['userinfo']['linkc'];
		
		$data['fb_app_id'] = '149756005087334';
		$this->load->view('userpage',$data);
		
		$this->load->view('bottom');
	
	}
	
	function forr($name,$round=""){
		
		
		
		// FIND OUT IF USER IS CONNECTED
		$data['fbconnection'] = $this->fbconnect(TRUE);
		// GET LOGIN URL
		$data['fbloginurl'] = $this->fbloginUrl($name);
		
		// IF CONNECTED FIND OUT IF VOTED
		$data['voted_for'] = "noone";
		if($data['fbconnection'] == 'yes'){
			
			$data['fbuid'] = $this->uid;
			
			$this->load->model('contestants');
			$data['voted_for'] = $this->contestants->getVote($data['fbuid']);
			
		}
		
		
		$data['nav'] = "home";
		$this->load->view('top', $data);
		
		$this->load->model('contestants');
		
		$data['contestants'] = $this->contestants->currentContestants();
		
		$this->load->view('vote',$data);
			
		$data['userinfo'] = $this->contestants->contestantData(strtolower($name));
		$data['userinfo']['namelink'] = str_replace(' ','_',$data['userinfo']['fname']);
		
		$data['round'] = $round;
		if($round == ""){
			$data['userinfo']['link'] = $data['userinfo']['linkc'];
		}
		else if($round == "round2"){
			$data['userinfo']['link'] = $data['userinfo']['linkb'];
		}
		else{
			$data['userinfo']['link'] = $data['userinfo']['link'];
		}
		
		$data['fb_app_id'] = '149756005087334';
		$this->load->view('userpage',$data);
		
		$this->load->view('bottom');

			
	}
	
	
	function userdata($id){
		//echo "user data";
		/*
		$this->load->model('contestants');
		
		$data['userinfo'] = $this->contestants->contestantData($id);

		//$data['username'] = "dan";
		
		$this->load->view('userpage',$data);
		*/
	}
	
	// finalists id in
	function record($id){
		// FIND OUT IF USER IS CONNECTED
		$data['fbconnection'] = $this->fbconnect(TRUE);
		
		
		if($data['fbconnection'] == 'yes'){
			
			$data['fbuid'] = $this->uid;
		
			if($data['fbuid'] != null && $data['fbuid'] != 0){
				
				// user logged in and we have id.
				$this->load->model('contestants');
				// check if exists in db - if so update vote
				$user_exists = $this->contestants->checkUser($data['fbuid']);
				
				if($user_exists){
					$this->contestants->updateVote($data['fbuid'],$id);
				}
				else{
					// else create user record with vote
					$this->contestants->createVote($data['fbuid'],$id);
				}
				
				$contestant_info = $this->contestants->contestantData($id);
				$name_link = str_replace(' ','_',$contestant_info['fname']);
				
				/* WALL PUBLISH ---------------------- */
				$fargs['message'] = " just voted for ".$contestant_info['name']." to be the Saskatchewanderer. Cast your vote!";
				$fargs['picture'] = "http://ca-socialmedia.com/saskw/images/finalists/".$contestant_info['photo'];
				$fargs['link'] = "http://saskatchewanderer.ca/vote/for/".$name_link."/";
				$fargs['name'] = "Saskatchewanderer";
				$fargs['caption'] = "Who do you think should be the Saskatchewanderer? Cast your vote!";
				$fargs['description'] = " ";
				
				try{
					//$data = $this->facebook->api('/me/feed', 'post', $fargs);
				}
				catch(Exception $e){
					error_log('wall publish error ' . $e);
				}
				/* ----------------------------------- */
				
				
				
			}
			else{
				error_log('error-900');
				echo "error";
			}
		
		}
		else{
			error_log('error-602');
			echo "error";
		}
	}
	
	
	
	function requestcancel(){
		// FIND OUT IF USER IS CONNECTED
		$data['fbconnection'] = $this->fbconnect(TRUE);
		
		
		if($data['fbconnection'] == 'yes'){
			
			$data['fbuid'] = $this->uid;
		
			if($data['fbuid'] != null && $data['fbuid'] != 0){
				
				// user logged in and we have id.
				$this->load->model('contestants');
				
				$this->contestants->cancelVote($data['fbuid']);
				
			}
			else{
				echo "error-900";
			}
		
		}
		else{
			echo "error-602";
		}

	}
	
	
	
	function fbconnect($connect_test=FALSE){
				
		//echo "conect";
		//Enter your Application Id and Application Secret keys
        $config['appId'] = '149756005087334';
        $config['secret'] = 'baf28c9605f0816229bcc178ef12911a';

        //Do you want cookies enabled?
        $config['cookie'] = true;
        
        //load Facebook php-sdk library with $config[] options
        $this->load->library('facebook', $config);

        //Load Session class for saving access token later
        $this->load->library('session');
        $this->session->sess_destroy();

        //Check to see if there is an active Facebook session.
        //We will not know if the session is valid until the call is made
        $session = $this->facebook->getSession();
        
        //Check to see if the access_token is present in Facebook session data
        if (!empty($session['access_token'])) {
            //Save token to Session data for later use
            
            $this->session->set_userdata('access_token', $session['access_token']);
             
        } else {
        	if($connect_test != TRUE){
				$this->fblogin();
        	}
        }
		
        try{
        	$data['api_info']  = $this->facebook->api('/me');
        	$uid_test = $this->facebook->getUser();
        }
        catch(Exception $e){
        	if($connect_test != TRUE){
        		$this->fblogin();
        	}
        	else{
        		return('no');
        	}
        }
        
        $this->uid = $this->facebook->getUser();
        return("yes");

	}
	
	
	
	
	
	
	private function fblogin(){
		//Extended Permissions requested from the API at time of login
		//$auth_config['req_perms'] = 'publish_stream';
		
		//Dialog Form Factors used to display login page
		$auth_config['display'] = 'page';
		
		//Callback URL once user is authenticated
		$auth_config['next'] = 'http://ca-socialmedia.com/saskw/?c=fbtest';
		
		//Get the login URL using the parameters in $auth_config array
		$login_url = $this->facebook->getLoginUrl($auth_config);
		header('Location: ' . $login_url);	
	}
	
	private function fbloginUrl($page_identifier){
	
		//Extended Permissions requested from the API at time of login
		//$auth_config['req_perms'] = 'publish_stream';
		
		//Dialog Form Factors used to display login page
		$auth_config['display'] = 'page';
		
		//Callback URL once user is authenticated
		$auth_config['next'] = 'http://ca-socialmedia.com/saskw/?c=fbtest&pid='.urlencode($page_identifier);
		
		//Get the login URL using the parameters in $auth_config array
		$login_url = $this->facebook->getLoginUrl($auth_config);
		
		return($login_url);	
	}
	
	
	
	
}

/* End of file vote.php */
/* Location: ./application/controllers/vote.php */