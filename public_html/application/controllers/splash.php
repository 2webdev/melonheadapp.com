<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect();
$layout = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'mobile') : 'desktop');

if($layout=='mobile')
{
	header('Location: http://melonheadapp.com/m/');	
}

class Splash extends CI_Controller {
	

	
	
	public function index()
	{
		
		
		
		// LOAD HTML
		
		$this->load->view('splash-top');
		$this->load->view('splash-initial');
		$this->load->view('splash-bottom');
		
	}
	
	
	
		
	
	
	
	
}

/* End of file badges.php */
/* Location: ./application/controllers/badges.php */