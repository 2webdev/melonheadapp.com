<?php

class Fbtest extends CI_Controller {

    function index() {
		
		//echo $_GET['pid'];
		
        //Enter your Application Id and Application Secret keys
        //$config['appId'] = '149756005087334';
        //$config['secret'] = 'baf28c9605f0816229bcc178ef12911a';
		
		$config['appId'] = $this->config->item('appId');
        $config['secret'] = $this->config->item('secret');

        //Do you want cookies enabled?
        $config['cookie'] = true;
        
        //load Facebook php-sdk library with $config[] options
        $this->load->library('facebook', $config);

        //Load Session class for saving access token later
        $this->load->library('session');
        $this->session->sess_destroy();

        //Check to see if there is an active Facebook session.
        //We will not know if the session is valid until the call is made
        $session = $this->facebook->getSession();
        
        //Check to see if the access_token is present in Facebook session data
        if (!empty($session['access_token'])) {
            //Save token to Session data for later use
            $this->session->set_userdata('access_token', $session['access_token']);
            //header('LOCATION:http://ca-socialmedia.com/saskw/votedev/forr/'.$_GET['pid'].'/');
             
        } else {
			$this->fblogin();
        }

        try{
        	$data['api_info']  = $this->facebook->api('/me');
        	$uid_test = $this->facebook->getUser();
        }
        catch(Exception $e){
        	$this->fblogin();
        }
        
        if(isset($_GET['pid'])){
	        if($_GET['pid'] == "home"){
	    		header('LOCATION:'.base_url().'app/index/');
	    	}
	    	else{
				header('LOCATION:'.base_url().'app/'.$_GET['pid'].'/');
	    	}
    	}
    	
        
        
        //Get the logout URL
        // $data['logout_url'] = $this->facebook->getLogoutUrl();

        //Load view to display results of API calls
        //$this->load->view('fbtest', $data);
    }    
    
    
    
    
    private function fblogin(){
    	  //Extended Permissions requested from the API at time of login
            $auth_config['req_perms'] = 'publish_stream,user_photos,user_photo_video_tags,user_location,email';

            //Dialog Form Factors used to display login page
            $auth_config['display'] = 'page';

            //Callback URL once user is authenticated
            $auth_config['next'] = base_url().'app/';

            //Get the login URL using the parameters in $auth_config array
            $login_url = $this->facebook->getLoginUrl($auth_config);
            header('Location: ' . $login_url);
            die();

    }
    
    
}

