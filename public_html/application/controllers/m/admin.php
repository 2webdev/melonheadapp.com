<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	

	
	
	public function index()
	{
		
				
		// Connect to DB
		$this->load->model('siast');
		
		// GRAB all users
		$data['badges'] = $this->siast->getBadges();
		
		// Display
		
		
		$this->load->library('pagination');

		$config['base_url'] = base_url().'admin/users/';
		$config['total_rows'] = '200';
		$config['per_page'] = '20'; 
		$config['enable_query_string'] = FALSE;
		
		$this->pagination->initialize($config); 
		
		echo $this->pagination->create_links();		
		
		
		// LOAD HTML
		/*
		$this->load->view('m/top',$data);
		$this->load->view('m/initial',$data);
		$this->load->view('m/bottom');
		*/
	}
	
	
	
		
	
	function users()
	{
		$this->load->library('pagination');

		$config['base_url'] = base_url().'admin/users/';
		$config['total_rows'] = '200';
		$config['per_page'] = '20'; 
		$config['enable_query_string'] = FALSE;
		
		$this->pagination->initialize($config); 
		
		echo $this->pagination->create_links();		
	}
	
	
	
	
}

/* End of file badges.php */
/* Location: ./application/controllers/badges.php */