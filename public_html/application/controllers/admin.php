<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	

	
	
	public function index()
	{
		$is_login = $this->session->userdata('is_login');
		
		if($is_login!='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		$this->load->model('siast');
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		$this->load->library('session');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() != FALSE)
		{
		  if($this->input->post('login')!=''){
			// $data['username']=$this->input->post('username'); 
			 //$data['password']=md5($this->input->post('password'));
			 $where_clause=array("username"=>$this->input->post('username'),'password'=>md5($this->input->post('password')));
			 $count= $this->siast->Retrive_single_records('admin',$where_clause,'','','','');
			
				if(count($count)>0)
				{
					$this->session->set_userdata('is_login',$this->input->post('username'));
					redirect(base_url().'admin/contest_list/', 'refresh');
				}
				else
				{
					
					$this->session->set_flashdata('err', 'Invalid Login');
					redirect(base_url().'admin/', 'refresh');	
				}
				
				
			
		  }
		}
		
			
		
		 $data['err']=$this->session->flashdata('err');
		
		
		// LOAD HTML
		$this->load->view('top',$data);
		$this->load->view('admin/login',$data);
		$this->load->view('bottom');		
		
	}
	
		function add_contest()
		{
			
			$is_login = $this->session->userdata('is_login');
			
			if($is_login=='')
			{
			redirect(base_url().'admin/', 'refresh');
			}
			
			 $this->load->model('siast');
			 
			if($this->input->post('update')!=''){
			 $row['status']='Inactive';
			 
			$row['content']=$this->input->post('content1');
			$row['title']=$this->input->post('title');
			
			$this->siast->Add_Record($row,'cms');
			
			 redirect(base_url().'admin/contest_list/', 'refresh');
			}
				// LOAD HTML
			$this->load->view('top',$data);
			$this->load->view('admin/add_contest',$data);
			$this->load->view('bottom');		
			
		}
	
	
		function contest_list()
		{
		$is_login = $this->session->userdata('is_login');
		
		if($is_login=='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		 $this->load->model('siast');
		  
		  
		$id= $this->uri->segment(3);
		
		if($id)
		{
			if($this->uri->segment(4)=='Active')
			{
				$status='Inactive';
				
			}
			else
			{
				$status='Active';
			}
			
			
			$data['status']=$status;
			$datan['status']=$this->uri->segment(4);
			 $where_clause=array("id"=>$id);
			 $where_clause_n=array("id"=>$id);
				$this->siast->Update_Record($data,'cms',$where_clause);
				$this->siast->Update_Record_not_in($datan,'cms',$id);
			 redirect(base_url().'admin/contest_list/', 'refresh');
		}
		$cms_row = $this->siast->Retrive_all_records('cms','','','','','');
		 
		 $data['cms_row'] = $cms_row;
		
		
		
		// LOAD HTML
		$this->load->view('top',$data);
		$this->load->view('admin/contest_list',$data);
		$this->load->view('bottom');
	}
	
	function user_list()
		{
			$this->load->model('siast');
			$all_user_arr =$this->input->post('id');
			//print_r($all_user_arr);
			if(count($all_user_arr)>0)
			{
				for($i=0;$i<count($all_user_arr);$i++)
				{
					
					$where_clause=array("id"=>$all_user_arr[$i]);
					$this->siast->Delete_records('contest',$where_clause,'','','','');
					 $all_user_arr[$i];
				}
			}
		$is_login = $this->session->userdata('is_login');
		
		if($is_login=='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		 
		  
		
			$cms_row = $this->siast->Retrive_all_records('cms','','','','','');
		 
		 $data['cms_row'] = $cms_row;
		$data['id']=  $id= $this->uri->segment(3);
		 if($id)
		 {
		 	$where_clause=array("cms_id"=>$id);
			$user_row = $this->siast->Retrive_all_records('contest',$where_clause,'','','','');
		 }
		 else
		 {
			 $user_row = $this->siast->Retrive_all_records('contest','','','','','');
		 }
		 $data['user_row'] = $user_row;
		


		
		
		// LOAD HTML
		$this->load->view('top',$data);
		$this->load->view('admin/user_list',$data);
		$this->load->view('bottom');
	}
	
	
	
	function edit_contest()
	{
		$is_login = $this->session->userdata('is_login');
		
		if($is_login=='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		 $this->load->model('siast');
		  $id= $this->uri->segment(3);
		  if($this->input->post('update')!=''){
			  
			 
			  
			 $data['content']=$this->input->post('content1');
			 $data['title']=$this->input->post('title');
			 $data['status']=$this->input->post('status');
			 $where_clause=array("id"=>$id);
				$this->siast->Update_Record($data,'cms',$where_clause);
			 redirect(base_url().'admin/contest_list/', 'refresh');
		  }
		
		 $where_clause=array("id"=>$id);
		
		$cms_row = $this->siast->Retrive_single_records('cms',$where_clause,'','','','');
		 
		 $data['title'] = $cms_row->title;
		 $data['content'] = $cms_row->content;
		$data['status'] = $cms_row->status;
		
		
		// LOAD HTML
		$this->load->view('top',$data);
		$this->load->view('admin/edit_contest',$data);
		$this->load->view('bottom');
	}
	
	function change_password()
	{
		 $is_login = $this->session->userdata('is_login');
		
		if($is_login=='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		$this->load->library('session');

	
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() != FALSE)
		{
			
		 $this->load->model('siast');
		  if($this->input->post('save')!=''){
			 $data['password']=md5($this->input->post('password'));
			 $where_clause=array("id"=>'1');
				$this->siast->Update_Record($data,'admin',$where_clause);
			 redirect(base_url().'admin/change_password/', 'refresh');
		  }
		
		}
		
		// LOAD HTML
		$this->load->view('top',$data);
		$this->load->view('admin/change_password',$data);
		$this->load->view('bottom');
	}
	
	function export_all_users($id)
	{
	
		$out = '';
		$fields = array("Name","Email","Location"," ","Date");
		
		
		//$columns = mysql_num_fields($fields);
		$columns = count($fields);
		// Put the name of all fields
		for ($i = 0; $i < $columns; $i++) {
		$l=$fields[$i];
		$out .= '"'.$l.'",';
		}
		$out .="\n";
		
		$j=0;
		 $this->load->model('siast');
		 if($id=='')
		 {
			 
			  $user_row = $this->siast->Retrive_all_records('contest','','','','','');
		 }
		 else
		 {
			 $where_clause=array("cms_id"=>$id);
		
			$user_row = $this->siast->Retrive_all_records('contest',$where_clause,'','','','');
		 }
		
	if(count($user_row)>0)
	{
		foreach($user_row as $key){	
		
		$out .=$key->user_name.','.$key->user_email.','.$key->user_location.','.date("Y-m-d",strtotime($key->created));
		$out .="\n";
		
		}
		
	}
	
		
		
		header("Content-type: text/x-csv");
		header("Content-Disposition: attachment; filename=export_all_users.csv");
		echo $out;
		exit;
	
	
	}
	
	function logout()
	{
		 $is_login = $this->session->userdata('is_login');
		
		if($is_login=='')
		{
			redirect(base_url().'admin/', 'refresh');
		}
		$this->session->unset_userdata('is_login');	
		redirect(base_url().'admin/', 'refresh');
	}
	
	function delete_contest($id)
	{
		
		 $this->load->model('siast');
		 $where_clause=array("id"=>$id);
		 $this->siast->Delete_records('cms',$where_clause,'','','','');
		 
		redirect(base_url().'admin/contest_list/', 'refresh');
	}
	
	function delete_user($id)
	{
		
		 $this->load->model('siast');
		 $where_clause=array("id"=>$id);
		 $this->siast->Delete_records('contest',$where_clause,'','','','');
		 
		redirect(base_url().'admin/user_list/', 'refresh');
	}
}

/* End of file badges.php */
/* Location: ./application/controllers/badges.php */